//
//  PekedexViewController.swift
//  tableviewsCocoapods
//
//  Created by david tayupanta on 5/1/18.
//  Copyright © 2018 david tayupanta. All rights reserved.
//

import UIKit

class PekedexViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, PokemonServiceDelegate {
    @IBOutlet weak var pokeData: UITableView!
    let pokemonService = PokemonService()
    var pokemonArray: [Pokemon] = []
    var selectedPokemon = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        pokemonService.delegate = self

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        pokemonService.get20FirstPokemon()
    }
    func first20Pokemon(_ pokemon: [Pokemon]) {
        pokemonArray = pokemon
        pokeData.reloadData()
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return pokemonArray.count
            
        default:
            return 5
            
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "pokemonCell") as! PokemonTableViewCell
        //cell.textLabel?.text = pokemonArray[indexPath.row].pkName
        cell.fillData(pokemon: pokemonArray[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        selectedPokemon = indexPath.row
        return indexPath
    }
   
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! PokemonDetailViewController
        destination.pokemon = pokemonArray[selectedPokemon]
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
