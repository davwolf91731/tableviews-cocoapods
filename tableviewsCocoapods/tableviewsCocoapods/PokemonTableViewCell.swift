//
//  PokemonTableViewCell.swift
//  tableviewsCocoapods
//
//  Created by david tayupanta on 5/1/18.
//  Copyright © 2018 david tayupanta. All rights reserved.
//

import UIKit

class PokemonTableViewCell: UITableViewCell {

    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func fillData(pokemon:Pokemon){
        idLabel.text = "\(pokemon.pkId ?? 0)"
        nameLabel.text = pokemon.pkName
    }
}
