//
//  ViewController.swift
//  tableviewsCocoapods
//
//  Created by david tayupanta on 27/11/17.
//  Copyright © 2017 david tayupanta. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class ViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func consultar(_ sender: Any) {
       
            
            Alamofire.request("https://pokeapi.co/api/v2/pokemon-form/50/").responseObject { (response: DataResponse<Pokemon>) in
                
                let pokemon = response.result.value
                DispatchQueue.main.async {
                    self.nameLabel.text = pokemon?.pkName
                    self.weightLabel.text = "\(pokemon?.pkWeight ?? 1)"
               self.heightLabel.text = "\(pokemon?.pkHeight ?? 1)"
                }
                print(pokemon?.pkName)
               
                
            }
            
           
        }
        
    
}

