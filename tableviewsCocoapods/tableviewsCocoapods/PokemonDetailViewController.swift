//
//  PokemonDetailViewController.swift
//  tableviewsCocoapods
//
//  Created by david tayupanta on 8/1/18.
//  Copyright © 2018 david tayupanta. All rights reserved.
//

import UIKit

class PokemonDetailViewController: UIViewController {
    var pokemon:Pokemon?

    @IBOutlet weak var pokemonImageView: UIImageView!
    @IBOutlet weak var version: UILabel!
    @IBOutlet weak var habilidad: UILabel!
    @IBOutlet weak var movimiento: UILabel!
    @IBOutlet weak var tipo: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = pokemon?.pkName
        self.version.text = pokemon?.pkVersion
        self.habilidad.text = pokemon?.pkHabilidad
        self.movimiento.text = pokemon?.pkMovimeinto
        self.tipo.text = pokemon?.pkTipo
        let pkService = PokemonService()
        pkService.getPokemonImage(id: (pokemon?.pkId)!) { (pkImage) in
            self.pokemonImageView.image = pkImage
            
        }
    }
}


